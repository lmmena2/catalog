# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in api_catalog/__init__.py
from api_catalog import __version__ as version

setup(
	name='api_catalog',
	version=version,
	description='Luis Mena ',
	author='Luis Mena',
	author_email='lmmena2@gmail.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
