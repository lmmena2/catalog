## ErpNext API Catalog

A product should have basic info such as sku, name, price and brand.


## Installation
1. You need frappe v12 installed

https://github.com/frappe/bench/blob/develop/docs/easy_install.md

2. Install the application (in a terminal)
    - bench start
    - bench get-app https://gitlab.com/lmmena2/catalog.git
    - bench install-app api_catalog

#### License

MIT





