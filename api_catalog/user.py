import frappe

SUCCESS=200
NOT_FOUND=400


#Obtener todos los usuarios
@frappe.whitelist(allow_guest = True)
def get_all_user():
    users=frappe.db.sql("""SELECT * FROM tabUser LIMIT 10""",as_dict=1)

    if(users):
        status_code=SUCCESS
        body= users 
    else:
        status_code=NOT_FOUND
        body="User not found"

    response=dict(
        status_code = status_code,
        body = body
    )

    return response


#Añadir usuarios nuevos
@frappe.whitelist(allow_guest = True)
def add_user(email,name,password,role):
    user_exist=check_user(email)

    #Si el usuario no existe retorna error
    if (user_exist==1):
        response=dict(
            status_code = NOT_FOUND,
            body = "The user already exists"
        )

    else:
    
    #Si no existe el usuario, se crea
        add_user=frappe.get_doc({
            "doctype":"User",
            "email":email,
            "first_name":name,
            "send_welcome_email":0,
            "new_password":password,
        })
        add_user.insert()
        frappe.db.commit()

    #Asignamos rol administrador o rol anonimo
        if (add_user.name):
            if (role=="Administrador"):
                add_role=frappe.get_doc({
                    "doctype":"Has Role",
                    "role":"System Manager",
                    "parent": email,
                    "parentfield":"roles",
                    "parenttype":"User"
                })
                add_role.insert()
                frappe.db.commit()
            else:
                add_role=frappe.get_doc({
                    "doctype":"Has Role",
                    "role":"Anonimo",
                    "parent": email,
                    "parentfield":"roles",
                    "parenttype":"User"
                })
                add_role.insert()
                frappe.db.commit()

        if (add_user.name):
            status_code=SUCCESS
            body="""Add User:""" +  add_user.full_name
        else:
            status_code=NOT_FOUND
            body="User not create"

        response=dict(
            status_code = status_code,
            body = body
        )

    return response


#Borrar usuarios

@frappe.whitelist(allow_guest = True)
def delete_user(email):
    user_exist=check_user(email)

    #Si existe el usuario se elimina
    if (user_exist==1):
        user=frappe.get_doc("User",email)
        user.db_set({
            "enabled":0
        })
        frappe.db.commit
        
        status_code=SUCCESS
        body="""User Delete""" 

    #Si no existe el retorna error
    else:
        status_code=NOT_FOUND
        body="Error deleting user"

    response=dict(
        status_code = status_code,
        body = body
    )

    return response

#Actualizar usuarios
@frappe.whitelist(allow_guest = True)
def update_user(email,name):
    user_exist=check_user(email)
    frappe.log_error(email,name)
    #Si existe se actualiza
    if (user_exist==1):
        user=frappe.get_doc("User",email)
        user.db_set({
            "first_name":name,
            "full_name":name
        })
        
        status_code=SUCCESS
        body="""User Update"""

    #Si no existe retorta error
    else:
        status_code=NOT_FOUND
        body="Error updating user"

    response=dict(
        status_code = status_code,
        body = body
    )

    return response



#Verificar si existe el usuario
def check_user(email):
    users=frappe.db.sql("""SELECT * FROM tabUser WHERE email='"""+email+"""' """,as_dict=1)
    flag=0
    if len(users)>0:
        flag=1
    else:
        flag=0
    
    return flag
    


