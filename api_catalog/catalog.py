import frappe
from frappe.core.doctype.communication.email import make

SUCCESS=200
NOT_FOUND=400


#Obtener todos los productos
@frappe.whitelist(allow_guest = True)
def get_all_items():
    items=frappe.db.sql("""SELECT * FROM tabCatalog""",as_dict=1)

    if(items):
        status_code=SUCCESS
        body= items 
    else:
        status_code=NOT_FOUND
        body="Items not found"

    response=dict(
        status_code = status_code,
        body = body
    )

    return response

#Añadir usuarios productos al catalogo
@frappe.whitelist(allow_guest = True)
def add_item(item_code,item_name,item_sku,item_price,item_brand):
    item_exist=check_item(item_code)

    #Si el item existe retorna error
    if (item_exist==1):
        response=dict(
            status_code = NOT_FOUND,
            body = "The item already exists"
        )

    else:
    
    #Si el item no existe, se crea
        add_item=frappe.get_doc({
            "doctype":"Catalog",
            "item_code":item_code,
            "item_name":item_name,
            "item_sku":item_sku,
            "item_price":item_price,
            "item_brand":item_brand
        })
        add_item.insert()
        frappe.db.commit() 
        status_code=SUCCESS
        body="Item added"

    response=dict(
        status_code = status_code,
        body = body
    )

    return response

#Borrar productos
@frappe.whitelist(allow_guest = True)
def delete_item(item_code):
    item_exist=check_item(item_code)
    frappe.log_error(item_code,item_code)
    #Si existe el item se elimina
    if (item_exist==1):
        frappe.db.sql("""DELETE FROM tabCatalog where item_code='"""+item_code+"""' """)
        frappe.db.commit
        
        status_code=SUCCESS
        body="""Item Deleted""" 

    #Si no existe el item retorna error
    else:
        status_code=NOT_FOUND
        body="Error deleting item"

    response=dict(
        status_code = status_code,
        body = body
    )

    return response

#Actualizar productos
@frappe.whitelist(allow_guest = True)
def update_item(item_code,item_name,item_sku,item_price,item_brand):
    
    #obtenemos precio inicio
    price=frappe.db.sql("""SELECT item_price FROM tabCatalog WHERE item_code='"""+item_code+"""' """,as_dict=1)
    init_price=price[0].item_price

    item_exist=check_item(item_code)

    #Si existe el producto se actualiza
    if (item_exist==1):
        frappe.db.sql("""UPDATE tabCatalog SET item_name='"""+item_name+"""',item_sku='"""+item_sku+"""', item_price='"""+item_price+"""',item_brand='"""+item_brand+"""' where item_code='"""+item_code+"""' """)
        status_code=SUCCESS
        body="""User Update"""

        #Notificacion que el precio original cambio

        if (item_price!=init_price):

            #obtener usuarios administradores
            admin_users=frappe.db.sql("""select tu.name,tu.email
                                        from  tabUser tu
                                        join `tabHas Role` tr on tr.parent=tu.name
                                        where tr.role='System Manager' and tu.enabled=1;""",as_dict=1)

            #Se crea el contenido del email
            content = """<h2>Cambio de precios en productos</h2>
                        <p>Estimado/a <br>Usuario<br>""" +""" El presente correo es para notificarle que el producto: """ + str(item_code)+""" nombre """ + str(item_name) + """ fue ACTUALIZADO. <br> <br> <br> 
                        Saludos Cordiales
                        </p> <br> <br>
                        """
            #Para las notificaciones es necesario cambiar el sender por un correo propio
            for x in admin_users:
                comm = make(						        
		        subject = "Cambio de precio en productos",
		        content = content,
		        sender = "laesperanza@gmail.com",
		        recipients = x.email,
		        communication_medium = "Email",
		        sent_or_received = "Sent",
		        send_email = True,
		        email_template = "Plantilla Comisiones")
                

    #Si no existe retorta error
    else:
        status_code=NOT_FOUND
        body="Error updating item"

    response=dict(
        status_code = status_code,
        body = body
    )

    return response

#Contador de visitas productos
@frappe.whitelist(allow_guest = True)
def consult_item(item_code):
    #obtnemos el numero de veces consultado
    init_counter = frappe.db.sql("""SELECT counter from tabCatalog where item_code='"""+item_code+"""' """,as_dict=1)


    if len(init_counter)>0:
        final_counter=int(init_counter[0].counter)+1
    else:
        final_counter=1

    #Actualizamos el contador
    frappe.db.sql("""UPDATE tabCatalog SET counter="""+final_counter+""" WHERE item_code='"""+item_code+"""' """)
    frappe.db.commit()


#Verifica si existe el producto
def check_item(item_code):
    items=frappe.db.sql("""SELECT * FROM tabCatalog WHERE item_code='"""+item_code+"""' """,as_dict=1)
    flag=0
    if len(items)>0:
        flag=1
    else:
        flag=0
    
    return flag