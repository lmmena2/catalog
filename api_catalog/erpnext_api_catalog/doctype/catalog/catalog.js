// Copyright (c) 2022, no and contributors
// For license information, please see license.txt

var variable_servidor = 'https://erp2.funerariajaramillo.com.ec/api/method/fj_inventario.Catalogo.'
frappe.ui.form.on('Catalog', {
	tipo : function(frm){
		if (cur_frm.doc.tipo == "BORRAR" || cur_frm.doc.tipo == "ACTUALIZAR"){
			fetch(variable_servidor +'get_all_items', {
			    method: 'GET',
			    headers: {
			        'Accept': 'application/json',
			        'Content-Type': 'application/json',		        
			    },    
			})
			.then(r => r.json())
			.then(r => {								
				cur_frm.clear_table("tabla_de_catalog");
				console.log(r.message)
				console.log(r.message.body)
				$.each(r.message.body, function (index, data) {
					var new_row = cur_frm.add_child("tabla_de_catalog")
			    	new_row.item_code = data.item_code
			    	new_row.item_name = data.item_name
			    	new_row.item_price = data.item_price
			    	new_row.item_sku = data.item_sku
			    	new_row.item_brand = data.item_brand
			   	})
			   	cur_frm.refresh_field("tabla_de_catalog")
			})			
		}
	},
	create: function(frm) {
		if (cur_frm.doc.item_code && cur_frm.doc.item_name && cur_frm.doc.item_sku && cur_frm.doc.item_price && cur_frm.doc.item_brand){

			fetch(variable_servidor +'add_item?item_code='+cur_frm.doc.item_code+'&item_name='+cur_frm.doc.item_name+'&item_sku='+cur_frm.doc.item_sku+'&item_price='+cur_frm.doc.item_price+'&item_brand='+cur_frm.doc.item_brand, {
			    method: 'GET',
			    headers: {
			        'Accept': 'application/json',
			        'Content-Type': 'application/json',		        
			    },    
			})
			.then(r => r.json())
			.then(r => {
				console.log(r)
			    if (r.message.status_code == 200){
			    	frappe.msgprint("Item creado con éxito !!!")
			    	cur_frm.set_value("item_code",undefined)
			    	cur_frm.set_value("item_name",undefined)
			    	cur_frm.set_value("item_sku",undefined)
			    	cur_frm.set_value("item_price",undefined)
			    	cur_frm.set_value("item_brand",undefined)
			    }
			    else{
			    	frappe.msgprint("Error al crear el item")
			    }
			})
		}
		else{
			frappe.msgprint("Ingrese por favor todos los datos del item")
		}
	},
	guardar: function(frm) {
		var row_selected=cur_frm.fields_dict.tabla_de_catalog.grid.get_selected_children()		
		if (row_selected.length > 1){
			frappe.msgprint("Por favor seleccione solo una fila")
		}
		else if  (row_selected.length == 0){
			frappe.msgprint("Por favor seleccione al menos una fila")
		}
		else if  (row_selected.length == 1){

			if (cur_frm.doc.tipo == "ACTUALIZAR"){

				fetch(variable_servidor +'update_item?item_code='+row_selected[0].item_code+'&item_name='+row_selected[0].item_name+'&item_sku='+row_selected[0].item_sku+'&item_price='+row_selected[0].item_price+'&item_brand='+row_selected[0].item_brand, {
				    method: 'GET',
				    headers: {
				        'Accept': 'application/json',
				        'Content-Type': 'application/json',		        
				    },    
				})
				.then(r => r.json())
				.then(r => {		
				console.log(r)			
				    if (r.message.status_code == 200){
				    	cur_frm.reload_doc()		    	
				    	frappe.msgprint("Item actualizado !!!")	
				    }
				    else{
				    	frappe.msgprint("Error al actualizar Item")
				    }
				})

			}
			else{
				
				fetch(variable_servidor +'delete_item?item_code='+row_selected[0].item_code, {
				    method: 'GET',
				    headers: {
				        'Accept': 'application/json',
				        'Content-Type': 'application/json',		        
				    },    
				})
				.then(r => r.json())
				.then(r => {					
				    if (r.message.status_code == 200){
				    	cur_frm.reload_doc()		    	
				    	frappe.msgprint("Item eliminado !!!")	
				    }
				    else{
				    	frappe.msgprint("Error al eliminar Item")
				    }
				})				
			}
			
		}

	}
});
