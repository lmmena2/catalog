// Copyright (c) 2022, no and contributors
// For license information, please see license.txt
var variable_servidor = 'https://erp2.funerariajaramillo.com.ec/api/method/fj_inventario.Catalog.'
frappe.ui.form.on('Visitas Catalog', {
	refresh: function(frm) {
		fetch(variable_servidor +'get_all_items', {
		    method: 'GET',
		    headers: {
		        'Accept': 'application/json',
		        'Content-Type': 'application/json',		        
		    },    
		})
		.then(r => r.json())
		.then(r => {								
			cur_frm.clear_table("tabla_de_catalog");			
			$.each(r.message.body, function (index, data) {
				var new_row = cur_frm.add_child("tabla_de_catalog")
		    	new_row.item_code = data.item_code
		    	new_row.item_name = data.item_name
		    	new_row.item_price = data.item_price
		    	new_row.item_sku = data.item_sku
		    	new_row.item_brand = data.item_brand
		    	new_row.id_tabla = data.name
		   	})
		   	cur_frm.refresh_field("tabla_de_catalog")
		})			
	}
});



frappe.ui.form.on('tabla de catalog visitas', {
	visitar_producto : function(frm,cdt,cdn){
		var item = locals[cdt][cdn]		
		fetch(variable_servidor +'consult_item?item_code='+item.item_code, {
		    method: 'GET',
		    headers: {
		        'Accept': 'application/json',
		        'Content-Type': 'application/json',		        
		    },    
		})
		.then(r => r.json())
		.then(r => {								
			frappe.set_route("Form", "Catalog", item.id_tabla);			
		})			

	},
})
