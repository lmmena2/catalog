// Copyright (c) 2022, no and contributors
// For license information, please see license.txt
var variable_servidor = 'https://erp2.funerariajaramillo.com.ec/api/method/fj_inventario.user.'

frappe.ui.form.on('Usuarios_Inventario', {
	tipo : function(frm){
		if (cur_frm.doc.tipo == "BORRAR" || cur_frm.doc.tipo == "ACTUALIZAR"){
			fetch(variable_servidor +'get_all_user', {
			    method: 'GET',
			    headers: {
			        'Accept': 'application/json',
			        'Content-Type': 'application/json',		        
			    },    
			})
			.then(r => r.json())
			.then(r => {								
				cur_frm.clear_table("tabla_de_usuarios_inventario");
				console.log(r.message)
				console.log(r.message.body)
				$.each(r.message.body, function (index, data) {
					var new_row = cur_frm.add_child("tabla_de_usuarios_inventario")
			    	new_row.nombre = data.first_name
			    	new_row.correo = data.email
			   	})
			   	cur_frm.refresh_field("tabla_de_usuarios_inventario")
			})			
		}
	},
	crear: function(frm) {
		if (cur_frm.doc.nombre && cur_frm.doc.email && cur_frm.doc.clave){

			fetch(variable_servidor +'add_user?email='+cur_frm.doc.email+'&name='+cur_frm.doc.nombre+'&password='+cur_frm.doc.clave+'&role='+cur_frm.doc.rol, {
			    method: 'GET',
			    headers: {
			        'Accept': 'application/json',
			        'Content-Type': 'application/json',		        
			    },    
			})
			.then(r => r.json())
			.then(r => {
				console.log(r)
			    if (r.message.status_code == 200){
			    	frappe.msgprint("Usuario creado con éxito !!!")
			    	cur_frm.set_value("nombre",undefined)
			    	cur_frm.set_value("email",undefined)
			    	cur_frm.set_value("clave",undefined)
			    }
			    else{
			    	frappe.msgprint("Error al crear usuario")
			    }
			})
		}
		else{
			frappe.msgprint("El nombre, correo y clave son obligatorios")
		}
	},
	guardar: function(frm) {
		var row_selected=cur_frm.fields_dict.tabla_de_usuarios_inventario.grid.get_selected_children()		
		if (row_selected.length > 1){
			frappe.msgprint("Por favor seleccione solo una fila")
		}
		else if  (row_selected.length == 0){
			frappe.msgprint("Por favor seleccione al menos una fila")
		}
		else if  (row_selected.length == 1){

			if (cur_frm.doc.tipo == "ACTUALIZAR"){

				fetch(variable_servidor +'update_user?email='+row_selected[0].correo+'&name='+row_selected[0].nombre, {
				    method: 'GET',
				    headers: {
				        'Accept': 'application/json',
				        'Content-Type': 'application/json',		        
				    },    
				})
				.then(r => r.json())
				.then(r => {					
				    if (r.message.status_code == 200){
				    	cur_frm.reload_doc()		    	
				    	frappe.msgprint("Usuario actualizado !!!")	
				    }
				    else{
				    	frappe.msgprint("Error al actualizar usuario")
				    }
				})

			}
			else{
				
				fetch(variable_servidor +'delete_user?email='+row_selected[0].correo, {
				    method: 'GET',
				    headers: {
				        'Accept': 'application/json',
				        'Content-Type': 'application/json',		        
				    },    
				})
				.then(r => r.json())
				.then(r => {					
				    if (r.message.status_code == 200){
				    	cur_frm.reload_doc()		    	
				    	frappe.msgprint("Usuario eliminado !!!")	
				    }
				    else{
				    	frappe.msgprint("Error al eliminar usuario")
				    }
				})				
			}
			
		}

	}
});





frappe.ui.form.on('tabla de usuarios inventario', {
	before_tabla_de_usuarios_inventario_remove : function(frm,cdt,cdn){
		frappe.throw(__("No se puede eliminar"))
	},
})