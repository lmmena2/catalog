from __future__ import unicode_literals
import unittest
from api_catalog.user import add_user,delete_user


class TestUtils(unittest.TestCase):
	@classmethod

	def test_add_user(self):
		user = add_user('luis.me@fuja.com.ec', 'Luis','123','Administrador')
		self.assertEqual(user, '_Test Add Users correct!!')
	def test_delete_user(self):
		user = delete_user('luis.me@fuja.com.ec')
		self.assertEqual(user, '_Test Delete Users correct!!')


# USER_RECORDS = [
# 	{
# 		"doctype": "User",
# 		"email": "luis.m@fuja.com.ec",
# 		"name": "Luis",
# 		"password": "1234",
# 		"role": "Administrador",
# 	},
	
# ]
